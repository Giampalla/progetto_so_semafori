#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semPost(){
    int fd = running->syscall_args[0];

 #ifdef DEBUG
    printf("[INFO] pid=%d chiama semPost con fd=%d\n", running->pid, fd);
#endif

    SemDescriptor* desc = SemDescriptorList_byFd(&running->sem_descriptors, fd);

    SYSCALL_ERROR_HANDLER(desc, DSOS_ENONTROVATO);

    Semaphore* sem = desc->semaphore;
    SemDescriptorPtr* desc_ptr;
    sem->count++;

    if (sem->count <= 0) {
        List_insert(&ready_list, ready_list.last, (ListItem*) running);
        desc_ptr = (SemDescriptorPtr*) List_detach(&sem->waiting_descriptors, (ListItem*) sem->waiting_descriptors.first);
        List_insert(&sem->descriptors, sem->descriptors.last, (ListItem*) desc_ptr);
        List_detach(&waiting_list, (ListItem*) desc_ptr->descriptor->pcb);
        running->status = Ready;
        running = desc_ptr->descriptor->pcb;
    }

    running->syscall_retvalue = 0;
    return;
}
