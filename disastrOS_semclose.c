#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semClose(){
    int fd = running->syscall_args[0];

#ifdef DEBUG                //gestione errori
    printf("[INFO] pid=%d chiama semClose con fd=%d\n", running->pid, fd);
#endif


    SemDescriptor* desc = SemDescriptorList_byFd(&running->sem_descriptors, fd);
	List_detach(&running->sem_descriptors, (ListItem*)desc);
	Semaphore* sem = desc->semaphore;
	SemDescriptorPtr* desc_ptr = (SemDescriptorPtr*) List_detach(&sem->descriptors, (ListItem*) desc->ptr);

    SYSCALL_ERROR_HANDLER(desc_ptr, DSOS_EDETACH);

    if (sem->descriptors.size == 0 && sem->waiting_descriptors.size == 0){
        List_detach(&semaphores_list, (ListItem*) sem);
        Semaphore_free(sem);
    }

	SemDescriptor_free(desc);
	SemDescriptorPtr_free(desc_ptr);

	running->syscall_retvalue = 0;
	return;

}
